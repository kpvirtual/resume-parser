const { CandidateResumes } = require("../models/candidate_resumes");
const { SkillsMaster, validate } = require("../models/skills_master");
const auth = require("../middleware/auth");
const ObjectId = require('mongodb').ObjectId
const express = require("express");
const parseIt = require('../utils/parseIt');
const router = express.Router();
const cors = require('cors');
const bodyParser = require('body-parser');
const multer = require('multer');
const path = require('path');
const ResumeParser = require('simple-resume-parser');
var opn = require('opn');
const app = express();
// enable CORS
app.use(cors());
// parse application/json
app.use(bodyParser.json());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));


// serving static files
app.use('/uploads', express.static('uploads'));

// handle storage using multer
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads');
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`);
    }
});
var upload = multer({ storage: storage });

// handle multiple file upload
router.post('/', upload.array('dataFiles', 10), (req, res, next) => {
    const files = req.files;
    if (!files || (files && files.length === 0)) {
        return res.status(400).send({ message: 'Please upload a file.' });
    }
    return res.send({ message: 'File uploaded successfully.', files });
});


router.get('/view/:filename', function (req, res) {
    //console.log(name); // should display 123
    const name = req.params.filename;
    //window.open("/uploads/"+name, "_blank");
    //res.redirect("./uploads/"+name);
    //window.open("http://localhost:8080/uploads/"+name,"open",true);
    opn("./uploads/" + name);
});

/**Get resumes by their names and save in compiled folder in object format**/
/*router.post('/uploaddoc', function (req, res, next) {
    var names = "resume11.docx,resume12.docx";
    var str_array = names.split(',')
    for(var i = 0; i < str_array.length; i++) {
      let path_upload='uploads/'+str_array[i];
      parseIt.parseResume(path_upload, './compiled');
   }
   return res.send({ message: 'Resumes object files created successfully.'});
  });*/

//router.post("/", async (req, res) => {
//    try {
//       // const { error } = validate(req.body);
//       // if (error) return res.status(400).send(error.details[0].message);

//        const resume = new CandidateResumes(req.body);
//        await resume.save();
//        res.send(resume);
//    } catch (error) {
//        console.log(error);
//        res.send("An error occured");
//    }
//});

router.get("/", async (req, res) => {
    try {
        const user_resume = await CandidateResumes.find();
        res.send(user_resume);
    } catch (error) {
        res.send("An error occured");
    }
});

// router.get('/search', async (req, res) => {
//     let search = null;
//     let searchbyskill = null;
// //console.log(req);
//     try {
//         // default search
//        if( req.query.length == 0 ){
//             const user_resume = await CandidateResumes.find({});
//             res.send(user_resume);
//        }
//        //by skills
//        if( req.query.skills?.length > 0 ){
//          const skill = req.query.skills.split(",");
//          const skillsregex =[]; 
//          skill.map((el) => {
//             let re = new RegExp(el.trim(),'i');
//                     skillsregex.push(re);
//                 });
//           searchbyskill = { skills: { $in: skillsregex } }
//        }
//        //by name
//        if( req.query.name?.length > 0 ){
//             const re = new RegExp(req.query.name.trim(),'i');
//             const searchbyname = { name: re }
//        }
//        //by objective
//        if( req.query.objective?.length > 0 ){
//             const search_objective = req.query.objective;

//             const searchbyojective = {
//                 $text:
//                 {
//                     $search: search_objective
//                 }
//             }
//         }

//         $or: [{ skills: 'A' }, { qty: { $lt: 30 } }]

//         //  search = {
//         //      $or: [ searchname, search1 ]
//         //      }
//        console.log(searchbyskill)

//        //run search
//         const user_resume = await CandidateResumes.find(
//             {search}
//           ).collation( { locale: 'en', strength: 2 } );
//         res.send(user_resume);
//     } catch (error) {
//         console.log(error);
//         res.send("An error occured");
//     }
// });


router.get('/search', async (req, res) => {
    //console.log(req);
    /** line commented by lokesh **/
    // let search = {};
    // test
    try {
        // default search
        if (req.query.length == 0) {
            const user_resume = await CandidateResumes.find({});
            res.send(user_resume);
        }

        //by skills
        let searchbyskill = null;
        if (req.query.skills?.length > 0) {
            const skill = req.query.skills.split(",");
            const skillsregex = [];
            skill.map((el) => {
                let re = new RegExp(el.trim(), 'i');
                skillsregex.push(re);
            });

            /** line commented by lokesh **/
            searchbyskill = { skills: { $in: skillsregex } }
            //searchbyskill = { skills: { $in: skill } }

        }

        //by name

        let searchbyname;

        if (req.query.name?.length > 0) {

            const re = new RegExp(req.query.name.trim(), 'i');

            searchbyname = { name: re }

        }

        //by objective

        let searchbyojective;

        if (req.query.objective?.length > 0) {

            const search_objective = req.query.objective;

            searchbyojective = {

                $text:

                {

                    $search: search_objective

                }

            }

        }

        $or: [{ status: 'A' }, { qty: { $lt: 30 } }]

        /** code added by lokesh **/
        //by email

        let searchbyemail;

        if (req.query.email?.length > 0) {
            const re = new RegExp(req.query.email.trim(), 'i');
            searchbyemail = { email: re }
        }

        let searchbyphone;

        if (req.query.phone?.length > 0) {

            const phoneno = new RegExp(req.query.phone.trim(), 'i');

            searchbyphone = { phone: phoneno }

        }



        let searchbycity;

        if (req.query.city?.length > 0) {

            const city = new RegExp(req.query.city.trim(), 'i');

            searchbycity = { place: city }

        }



        let searchbycompany;

        if (req.query.company?.length > 0) {

            const company = new RegExp(req.query.company.trim(), 'i');

            searchbycompany = { workExperience : company }

        }

        // search = {
        //     $or: [ searchname, search1 ]
        //     }

        let search = Object.assign({}, searchbyskill, searchbyname, searchbyemail, searchbyojective, searchbyphone,searchbycity,searchbycompany)

        //run search
        const user_resume = await CandidateResumes.find(

            search

        ).collation({ locale: 'en', strength: 2 });

        const skillList = await SkillsMaster.find();

        const finalResponse = {
            userResumes:user_resume,
            skills:skillList,

        }
        res.send(finalResponse);

    } catch (error) {

        console.log(error);

        res.send("An error occured");

    }

});

router.get("/:id", async (req, res) => {

    try {
        var id = req.params.id;
        const user_resume = await CandidateResumes.findOne({ _id: ObjectId(req.params.id) });
        res.send(user_resume);
    } catch (error) {
        console.log(error);
        res.send("An error occured");
    }
});

router.post("/parse", async (req, res) => {
    if (req.body.filename) {
        var names = req.body.filename;
        var str_array = names.split(',')
        for (var i = 0; i < str_array.length; i++) {
            let path_upload = 'uploads/' + str_array[i];
            parseIt.parseResume(path_upload, './compiled');
        }
    }
    //return res.send(req.body.filename);
    res.status(200).json({ message: 'Data Added Successfully' });
});

module.exports = router;
