const express = require("express");
const router = express.Router();
const { Client } = require('@elastic/elasticsearch')
const client = new Client({
  node: process.env.ES_URL,
  auth: {
    username: process.env.ES_USERNAME,
    password: process.env.ES_PASSWORD
  }
});
router.post("/save", async (req, res) => {
    try {
       // const { error } = validate(req.body);
       // if (error) return res.status(400).send(error.details[0].message);

    } catch (error) {
        console.log(error);
        res.send("An error occured");
    }
});
 
router.get("/search", async (req, res) => {
    try {
        const result = await client.search({
            index: process.env.ES_INDEX,
            body: {
                "query":{"match":{"firstName.parts.summary":{"query":"REACTJS"}}}
            }
          });
          console.log(result);
          res.send(result);
    } catch (error) {
        console.log(error);
        res.send("An error occured");
    }
});



module.exports = router;
