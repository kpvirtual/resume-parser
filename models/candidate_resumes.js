const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const jwt = require("jsonwebtoken");
const Joi = require("joi");
const { array, object } = require("joi");
const opts = {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  };
const resumeSchema = new Schema({
    name: {
        type: String
    },
    email: {
        type: String
    },
    phone: {
        type: String
    },
    objective:{
        type : String
    },
    resumePath:{
        type : String
    },
    totalYearsExperience : {
        type : Object
    },
    place:{},
    skills:[],
    degree:[],
    designition:[],
    education:[],
    university:[],
    workExperience:[],
    raw_resume:[],
    
    
},opts);

// resumeSchema.methods.generateAuthToken = function () {
//     const token = jwt.sign(
//         { _id: this._id, name: this.name },
//         process.env.JWTPRIVATEKEY
//     );
//     return token;
// };

const CandidateResumes = mongoose.model("candidate_resumes", resumeSchema);

// const validate = (resume) => {
//     const schema = Joi.object({
//         name: Joi.string().required(),
//         email: Joi.string().email().required(),
//         phone: Joi.string().required(),
//     });
//     return schema.validate(resume);
// };

module.exports = { CandidateResumes };
