const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const opts = {
  timestamps: {
    createdAt: "created_at",
    updatedAt: "updated_at",
  },
};
const Skills_Master = new Schema(
  {
    value: {
      type: String,
    },
    label: {
      type: String,
    },
  },
  opts
);

const SkillsMaster = mongoose.model("skills_master", Skills_Master);

module.exports = { SkillsMaster };
