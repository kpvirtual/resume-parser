var ParseBoy = require('../src/ParseBoy');
var processing = require('../src/libs/processing');
var _ = require('underscore');
const { CandidateResumes } = require("../models/candidate_resumes");
const ObjectId = require('mongodb').ObjectId
var logger = require('tracer').colorConsole();

var parser = {
  /*parseResume: function (file,savePath) {
    var objParseBoy = new ParseBoy(), savedFiles = 0;


    var onFileReady =  function (preppedFile) {
      objParseBoy.parseFile(preppedFile, function (Resume) {
        logger.trace('I got Resume for ' + preppedFile.name + ', now saving...');
        const resume = new CandidateResumes(Resume.parts);
                       resume.save();
         objParseBoy.storeResume(preppedFile, Resume, savePath, function (err) {
          if (err) {
            return logger.error('Resume ' + preppedFile.name + ' errored',err);
          }
          logger.trace('Resume ' + preppedFile.name + ' saved');

        })
      });
    }
    processing.run(file, onFileReady);
  }*/
  parseResume: function (file,savePath) {
    var resumeLink={}
    var objParseBoy = new ParseBoy(), savedFiles = 0;
    var onFileReady = function (preppedFile) {
      objParseBoy.parseFile(preppedFile, function (Resume) {
        logger.trace('I got Resume for testing resumeLink' + preppedFile.name + ', now saving...');
        resumeLink.resumePath=preppedFile.name;
        let resumeData=Object.assign(Resume.parts, resumeLink)
        const resume = new CandidateResumes(resumeData);
        resume.save();
        objParseBoy.storeResume(preppedFile, Resume, savePath, function (err) {
          if (err) {
            return logger.error('Resume ' + preppedFile.name + ' errored',err);
          }
          logger.trace('Resume ' + preppedFile.name + ' saved'); 
        })
      });
    }
    processing.run(file, onFileReady);
  }

}
module.exports = parser;
