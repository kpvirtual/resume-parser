require("dotenv").config();
const users = require("./routes/users");
const auth = require("./routes/auth");
const resumes = require("./routes/resumes");
//const es_search = require("./routes/es_search");
//const search = require("./routes/search");
const connection = require("./db");
const cors = require("cors");
const express = require("express");
const app = express();

connection();

app.use(express.json());
app.use(cors());

app.use("/api/users", users);
app.use("/api/auth", auth);
app.use("/api/resumes", resumes);
app.use("/api/resumes/search", resumes);

//app.use("/api/es_search", es_search);
//app.use("/api/search", search);
app.use("/api/resumes/parse", resumes);
app.use("/api/resumes/view", resumes);

const port = process.env.PORT || 8080;
app.listen(port, () => console.log(`Listening on port ${port}...`));
